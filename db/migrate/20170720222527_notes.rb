class Notes < ActiveRecord::Migration[5.1]
  def change
    create_table :notes do |t|
        t.string :title, limit: 40, null: false
        t.boolean :category, default: false, null: false
        t.string :description, limit: 1000
        t.date :deadline
		t.timestamps
    end
  end
end