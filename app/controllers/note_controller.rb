class NoteController < ApplicationController
	def index
		@notes = Note.all.group_by { |n| n.created_at.to_date }
	end
	
	def show
		@note = Note.find(params[:id])
	end
	
	def new
		@note = Note.new
	end
	
	def create
		@note = Note.new(note_params)
		
		if (@note.category == 1 && !@note.deadline)
			@note.deadline = nil
		end
		
		if @note.save
			flash[:success] = 'Successfully added note!'
			redirect_to note_index_path
		else
			flash[:warning] = 'Failed to add note.'
			redirect_to new_note_path
		end
	end
	
	def edit
		@note = Note.find(params[:id])
	end
	
	def update
		@note = Note.find(params[:id])
		
		if (@note.category == 1 && !@note.deadline)
			@note.deadline = nil
		end
		
		if @note.update_attributes(note_params)
			flash[:success] = 'Note updated!'
			redirect_to note_path
		else
			flash[:warning] = 'Failed to update note.'
			redirect_to edit_note_path
		end
	end
	
	def destroy
		if Note.find(params[:id]).destroy
			flash[:success] = 'Note deleted!'
			redirect_to note_index_path
		else
			flash[:warning] = 'Failed to ddelete note.'
			redirect_to note_path
		end
	end
	
	private
		def note_params
			params.require(:note).permit(:title, :category, :description, :deadline)
		end
end
