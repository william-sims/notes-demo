class Note < ApplicationRecord
	validates_presence_of :title, :message=>"Title field cannot be empty."
	validates_length_of :title, :within => 1..40, :message=>"Invalid Title length."
	validates_presence_of :description, :message=>"Invalid Description length."
	validates :description, length: {maximum: 1000}
	#
end