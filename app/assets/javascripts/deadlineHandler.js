document.addEventListener("turbolinks:load", function() {
	$( document ).ready(function() {
		$('#deadline_label').hide();
		$('#note_deadline_1i').hide();
		$('#note_deadline_2i').hide();
		$('#note_deadline_3i').hide();
		$('#category').change(function() {
			if ($('#category').val() == 0) {
				$('#deadline_label').hide();
				$('#note_deadline_1i').hide();
				$('#note_deadline_2i').hide();
				$('#note_deadline_3i').hide();
				$('#note_deadline_1i').val('Select year');
				$('#note_deadline_2i').val('Select month');
				$('#note_deadline_3i').val('Select day');
			} else {
				$('#deadline_label').show();
				$('#note_deadline_1i').show()
				$('#note_deadline_2i').show()
				$('#note_deadline_3i').show()
			}
		});
	});
})
